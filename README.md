# xsql-maker

#### 介绍
XSql注解处理器<br>
为xsql数据库自动生成数据库模型实体java类的映射关系类。
最新版本 [![](https://jitpack.io/v/com.gitee.pichs/xsql-maker.svg)](https://jitpack.io/#com.gitee.pichs/xsql-maker)

    ```
      1. kotlin请使用
      kapt  'com.gitee.pichs:xsql-maker:1.1'
      
      或
      
      2. java请使用
      annotationProcessor 'com.gitee.pichs:xsql-maker:1.1'
     
        
      3. 一般还会用到xsql-core
      implementation 'com.gitee.pichs:xsql-core:1.1'
     
    ```
    
### 传送门： [使用文档](https://gitee.com/pichs/xsql-core)
